import jester, asyncdispatch, htmlgen, json, os, httpclient, strutils, nre

# General settings
var settings = newSettings()
if existsEnv("PORT"):
  settings.port = Port(parseInt(getEnv("PORT")))

# Begin hmmm
const HMM_SRC = "https://www.reddit.com/r/hmmm.json"
proc getHmmm(): string =
  var client = newHttpClient()
  let json = parseJson client.getContent(HMM_SRC)
  result = json["data"]["children"][0]["data"]["url"].getStr()

# Begin pivotal
const PIVOTAL_DOMAIN = "pivotaltracker.com"
const GET_ENDPOINT   = "https://www.pivotaltracker.com/services/v5/projects/$#/stories/$#"

const STATE_COLORS = {
  "accepted":    "#2CA58D",
  "delivered":   "#FFA630",
  "finished":    "#D7E8BA",
  "started":     "#4D7EA8",
  "rejected":    "#F64740",
  "planned":     "#828489",
  "unstarted":   "#828489",
  "unscheduled": "#828489"
}.toTable

proc unfurlPivotal(pivotal_id: string): JsonNode =
  var client = newHttpClient()
  client.headers = newHttpHeaders({"X-TrackerToken": getEnv("PIVOTAL_TOKEN")})

  try:
    let
      uri         = GET_ENDPOINT % [getEnv("PIVOTAL_PROJECT_ID"), pivotal_id]
    echo "[Pivotal] GET: " & uri
    let
      response    = parseJson client.getContent(uri)
      title       = response["name"].getStr()
      state       = response["current_state"].getStr()
      state_color = STATE_COLORS[state]

    result = %* {
      "title": title & " [" & state & "]",
      "color": state_color
    }
  except HttpRequestError:
    result = newJNull()

proc getStoryID(link: JsonNode): string =
  result = ""
  var regex = re"/story/show/(\d+)$"
  if link["domain"].getStr() == PIVOTAL_DOMAIN:
    let url = link["url"].getStr()
    result = url.find(regex).get.captures[0]

const UNFURL_URI = "https://slack.com/api/chat.unfurl"
proc doUnfurl(unfurled, body: JsonNode) =
  var
    client = newHttpClient()
    data   = newMultipartData()

  client.headers = newHttpHeaders({
    "Content-Type": "application/json;charset=utf-8"
  })

  data["token"]   = getEnv("SLACK_OAUTH")
  data["channel"] = body["event"]["channel"].getStr()
  data["ts"]      = body["event"]["message_ts"].getStr()
  data["unfurls"] = $unfurled

  echo client.postContent(UNFURL_URI, multipart=data)

proc unfurlPivotal(body: JsonNode) =
  let event = body["event"]
  var unfurled = newJObject()
  for link in event["links"]:
    let
      id = getStoryID(link)
      uri = link["url"].getStr()
    if id != "":
      let attachment = unfurlPivotal(id)
      if attachment.kind != JNull:
        unfurled.add uri, attachment
  doUnfurl(unfurled, body)

# web app
const JSON = "application/json;charset=utf-8"

routes:
  get "/":
    resp h1("go away")
  post "/hmmm":
    let token = @"token"
    if token == getEnv("SLACK_TOKEN"):
      let payload = %* {
        "response_type": "in_channel",
        "attachments": [
          {"image_url": getHmmm()}
        ]
      }
      resp $payload, JSON
    else:
      resp Http401, "App is not authorized."
  post "/pivotal":
    let body = parseJson(request.body)
    let token = body["token"].getStr()
    if token == getEnv("SLACK_TOKEN"):
      if body["type"].getStr() == "url_verification":
        resp body["challenge"].getStr()
      else:
        unfurlPivotal(body)
        resp Http200, ""
    else:
      resp Http401, "App is not authorized."

runForever()
