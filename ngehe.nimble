# Package

version     = "0.1.0"
author      = "Tapio Saarinen"
description = "Shitty slack bot"
license     = "MIT"
bin         = @["ngehe"]

# Dependencies

requires "nim >= 0.17.0"
requires "jester"
