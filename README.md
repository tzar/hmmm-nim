# README #

Two equally important functions. Provides `/hmmm` to post the top image from /r/hmmm to the channel. Also captures links to pivotal stories and unfurls them.

## Hosting on heroku

- Use this buildpack: https://github.com/tzar/heroku-buildpack-nim

## Required config

The following env vars are necessary:

- `BUILD_FLAGS="-d:ssl -d:release"`
- `PIVOTAL_PROJECT_ID` set to your pivotal ID (sadly their API doesn't allow querying stories without a project ID included, but also the shareable link doesn't include the project ID)
- `PIVOTAL_TOKEN` - token used to authenticate to pivotal to get data
- `SLACK_TOKEN` - token for the / slash command
- `SLACK_OAUTH` - oauth token for link unfurling

In slack, you need to make an app and add a slash command pointed at http://your-deployment/hmmm . You also need to enable the event API and give it the ability to rewrite links.